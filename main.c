#include <msp430.h>

//#define USER_PRINTF


void initUART(void);
void fputc(unsigned byte);
void printf(char *format, ...);

void main(void)
{
    WDTCTL = WDTPW + WDTHOLD;           // Stop WDT

    initUART();

    // Initialize values to display
    char *s = "NJC's MSP430 LaunchPad Blog";
    char c = '!';
    int i = -12345;
    unsigned u = 4321;
    long int l = -123456780;
    long unsigned n = 1098765432;
    unsigned x = 0xABCD;

    while(1)
    {
        printf("String         %s\r\n", s);
        __delay_cycles(100000);
        printf("Char           %c\r\n", c);
        __delay_cycles(100000);
        printf("Integer        %i\r\n", i);
        __delay_cycles(100000);
        printf("Unsigned       %u\r\n", u);
        __delay_cycles(100000);
        printf("Long           %l\r\n", l);
        __delay_cycles(100000);
        printf("uNsigned loNg  %n\r\n", n);
        __delay_cycles(100000);
        printf("heX            %x\r\n", x);
        __delay_cycles(100000);

    }
}


/**
 * Initializes the UART for 9600 baud with a RX interrupt
 **/
void initUART(void) {
    P1SEL = BIT5 + BIT6;
    UCA0CTL1 |= UCSWRST;
    UCA0CTL1 |= UCSSEL__SMCLK; //SMLK
    UCA0BRW = 109;
    //UCA0MCTL = 12;

    UCA0CTL1 &= ~UCSWRST;

    //UCA0IE = UCRXIE;
}







